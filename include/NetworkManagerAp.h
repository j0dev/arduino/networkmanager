#pragma once

#ifdef ESP32
    #include <WiFi.h>
#endif
#ifdef ESP8266
    #include <ESP8266WiFi.h>
#endif

#include <Arduino.h>
#include <ArduinoJson.h>

#include "nmtypes.h"
#include "config.h"
#define NMAP_CONFIG_CAP 20 // 9*2 + 2 string config options
#define NMAP_CONFIG_FILE "/_nm_ap.json" // currently using _ instad of / because of issues

/**
 * NetworkManagerAp contains all SoftAP functionality
 */
class NetworkManagerAp {
    public:
        /**
         * apEnable enables the SoftAP
         */
        void apEnable();
        /**
         * apDisable disables the SoftAP
         */
        void apDisable();
        /**
         * isApEnabled returns if the SoftAP is enabled
         */
        bool isApEnabled();


        /**
         * apEnableReconf lets the SoftAP configuration be changed from the config file
         */
        void apEnableReconf();
        /**
         * apDisableReconf lets the SoftAP configuration not changed from the config file
         */
        void apDisableReconf();
        /**
         * isApReconfEnabled returns the current state of configuration overrides by the config file
         */
        bool isApReconfEnabled();


        /**
         * setup AP system
         */
        void apBegin();
        /**
         * Loop function required for some functionality
         */
        void apLoop();
        /**
         * apStart starts the SoftAP functionality
         */
        void apStart();


        /**
         * setApSSID sets the name of the SoftAP network (SSID)
         */
        void setApSSID(String ssid);
        /**
         * getApSSID retrieves the configured SSID of the SoftAP
         */
        String getApSSID();
        /**
         * setApPSK sets the password of the SoftAP network (wpa2-psk)
         */
        void setApPSK(String psk);
        /**
         * getApPSK retrieves the configured PSK of the SoftAP
         */
        String getApPSK();


        /**
         * setApStaticIP sets a static ip on the SoftAP interface
         * @note Please note that some boards have issues with non /24 subnetmasks
         */
        void setApStaticIP(IPAddress ip, IPAddress gw, IPAddress sn);
        /**
         * unsetApStaticIP resets the ip configuration back to its standard configuration
         */
        void unsetApStaticIP();

        /**
         * getApIp returns the configured static IP address
         */
        IPAddress getApIp();
        /**
         * getApGw returns the configured static gateway
         */
        IPAddress getApGw();
        /**
         * getApSn returns the configured static subnet mask
         */
        IPAddress getApSn();
        /**
         * isApStaticIpEnabled returns if static IP configuration is enabled
         */
        bool isApStaticIpEnabled();


        /**
         * setApTimeout sets the timeout for how long the SoftAP stays up when another network is active
         * after this timeout the SoftAP gets disabled
         * This can be used to reconfigure without using the configured network
         * SoftAP will always activate when no connection to other networks can be made
         * Set to 0 to disable
         */
        void setApTimeout(uint timeout);
        /**
         * getApTimeout returns the configured timeout
         */
        uint getApTimeout();
        /**
         * isApTimeoutActive returns if the timeout system is active
         */
        bool isApTimeoutActive();
        /**
         * isApTimeoutRunning retuns if the timeout is running
         */
        bool isApTimeoutRunning();

        /**
         * stopApTimeout stops the configured timeout
         * This gets called when a user accesses the configuration system from the SoftAP
         */
        void stopApTimeout();
        /**
         * resetApTimeout reset / re-enables the SoftAP timeout
         * This gets called when reconfiguration is done
         */
        void resetApTimeout();


        /**
         * apLoadConfig reads the config from storage
         * Note: this does not apply restart the interfaces with the new configuration
         * Note: the timeout (if running and active) does take the new configuration
         */
        bool apLoadConfig();
        /**
         * ApLoadJson allows overriding of (most) configuration options from a json document
         * @note Non present options will be ignored and keep their current configuration
         */
        void apLoadJson(JsonVariant conf);
        /**
         * apGetJson retrieves the json configuration
         */
        void apGetJson(JsonDocument& doc);
        /**
         * apWriteConfig writes the current configuration for persistance between boots
         */
        bool apWriteConfig();

        virtual const char* getHostname() { return ""; };

        virtual bool isModeActive(NM_MODE_ENUM mode) { return false; };

    protected:
        bool apEnabled = true;         // Is the SoftAP enabled

        String apSSID;                 // Network name
        String apPSK;                  // Network password

        bool apStaticIp = false;       // Static network config?
        IPAddress apIP;                // IP address
        IPAddress apGW;                // GateWay address
        IPAddress apSN;                // SubNetmask

        bool apAllowReconf = true;     // Allow reconfiguration of SoftAP // password is always reconfigurable

        uint apTimeout;                // Timeout (in seconds)
        bool apTimeoutActive = false;  // Is timeout enabled
        bool apTimeoutRunning = false; // is timeout running
        ulong apTimeoutStart;          // Timeout start time

        virtual void setMode(NM_MODE_ENUM mode, bool state) {};
};
