#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include "LittleFS.h"

bool fs_init_begin();

bool fs_read_json_doc(String filename, JsonDocument *doc);
bool fs_read_json_doc(String folder, String filename, JsonDocument *doc);

bool fs_write_json_doc(String filename, JsonDocument *doc);
bool fs_write_json_doc(String folder, String filename, JsonDocument *doc);