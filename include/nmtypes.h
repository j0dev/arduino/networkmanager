#pragma once

enum NM_MODE_ENUM
{
    AP = 1,
    STA = 2,
    ETH = 4,
    WEB = 8,
    WEBUI = 16
};