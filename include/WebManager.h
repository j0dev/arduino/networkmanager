#pragma once

#include <ESPAsyncWebServer.h>
#include "NetworkManagerAp.h"
#include "NetworkManagerSta.h"

class NetworkWebManager {
    public:
        /**
         * webInit sets up and enables the network web manager
         * @param server Webserver object
         * @param port Port that the webserver is bound to
         * @param bindRoot if the root should be redirected to the network manager
         */
        void webInit(AsyncWebServer *server, uint16_t port, bool bindRoot);

        virtual const char *getHostname() { return ""; };

    protected:
        uint16_t port;
        AsyncWebServer* server;

        NetworkManagerAp *WebApPointer;
        NetworkManagerSta *WebStaPointer;

        void webPointerInit(NetworkManagerAp *Ap, NetworkManagerSta *Sta);
        void webSetup(bool bindRoot);

        #ifndef NM_WEBUI_DISABLED

        void webHandlePageRoot(AsyncWebServerRequest *request);
        void webHandlePageAp(AsyncWebServerRequest *request);
        void webHandlePageSta(AsyncWebServerRequest *request);

        #endif

        void webHandleConfigApRead(AsyncWebServerRequest *request);
        void webHandleConfigApWrite(AsyncWebServerRequest *request, JsonVariant json);

        void webHandleConfigStaRead(AsyncWebServerRequest *request);
        void webHandleConfigStaWrite(AsyncWebServerRequest *request, JsonVariant json);
};