#pragma once

#ifdef ESP32
    #include <WiFi.h>
    #ifdef NM_EAP_ENABLED
        #include "esp_wpa2.h"
    #endif
#endif
#ifdef ESP8266
    #include <ESP8266WiFi.h>
    #ifdef NM_EAP_ENABLED
        extern "C" {
            #include "user_interface.h"
            #include "wpa2_enterprise.h"
        }
    #endif
#endif

#include <Arduino.h>
#include <ArduinoJson.h>

#include "nmtypes.h"
#include "config.h"
#define NMSTA_CONFIG_CAP 21              // 9*2 + 3 string config options
#define NMSTA_CONFIG_FILE "/_nm_sta.json" // currently using _ instad of / because of issues

/**
 * NetworkManagerSta contains all WiFi Station / client functionality
 */
class NetworkManagerSta
{
    public:
        /**
         * staEnable enables the WiFi client
         */
        void staEnable();
        /**
         * staDisable disables the WiFi client
         */
        void staDisable();
        /**
         * isStaEnabled returns if the WiFi client is enabled
         */
        bool isStaEnabled();

        /**
         * staEnableReconf lets the WiFi client configuration be changed from the config file
         */
        void staEnableReconf();
        /**
         * staDisableReconf lets the WiFi client configuration not changed from the config file
         */
        void staDisableReconf();
        /**
         * isStaReconfEnabled returns the current state of configuration overrides by the config file
         */
        bool isStaReconfEnabled();


        /**
         * setup STA system
         */
        void staBegin();
        /**
         * Loop function required for some functionality
         */
        void staLoop();
        /**
         * staStart starts the WiFi client functionality
         */
        void staStart();


        /**
         * setStaSSID sets the name of the WiFi client network (SSID)
         */
        void setStaSSID(String ssid);
        /**
         * getStaSSID retrieves the configured SSID of the WiFi client
         */
        String getStaSSID();
        /**
         * setStaPass sets the password of the WiFi client network (wpa2-psk / eap password)
         */
        void setStaPass(String pass);
        /**
         * getStaPass retrieves the configured password of the WiFi client
         */
        String getStaPass();
        /**
         * setStaUser sets the username of the WiFi client network (eap username) and enables WPA2-EAP authentication
         */
        void setStaUser(String user);
        /**
         * unsetStaUser unsets the username and disables WPA2-EAP authentication
         */
        void unsetStaUser();
        /**
         * getStaUser retrieves the configured username of the WiFi client
         */
        String getStaUser();
        /**
         * hasStaUser returns if a username is set and subsequently if WPA2-EAP authentication is enabled
         */
        bool hasStaUser();


        /**
         * setStaStaticIP sets a static ip on the WiFi client interface
         * @note Please note that some boards have issues with non /24 subnetmasks
         */
        void setStaStaticIP(IPAddress ip, IPAddress gw, IPAddress sn);
        void setStaStaticIP(IPAddress ip, IPAddress gw, IPAddress sn, IPAddress dns);
        /**
         * unsetStaStaticIP resets the ip configuration back to its standard configuration
         */
        void unsetStaStaticIP();
        /**
         * getStaIp returns the configured static IP address
         */
        IPAddress getStaIp();
        /**
         * getStaGw returns the configured static gateway
         */
        IPAddress getStaGw();
        /**
         * getStaSn returns the configured static subnet mask
         */
        IPAddress getStaSn();
        /**
         * getStaDns returns the configured static dns server
         */
        IPAddress getStaDns();
        /**
         * isStaStaticIpEnabled returns if static IP configuration is enabled
         */
        bool isStaStaticIpEnabled();


        /**
         * staLoadConfig reads the config from storage
         * Note: this does not apply restart the interfaces with the new configuration
         * Note: the timeout (if running and active) does take the new configuration
         */
        bool staLoadConfig();
        /**
         * staLoadJson allows overriding of (most) configuration options from a json document
         * @note Non present options will be ignored and keep their current configuration
         */
        void staLoadJson(JsonVariant conf);
        /**
         * staGetJson retrieves the json configuration
         */
        void staGetJson(JsonDocument& doc);
        /**
         * staWriteConfig writes the current configuration for persistance between boots
         */
        bool staWriteConfig();


        virtual const char *getHostname() { return ""; };

        virtual bool isModeActive(NM_MODE_ENUM mode) { return false; };

    protected:
        bool staEnabled = false;

        String staSSID;
        String staPass;
        String staUser;

        bool staStaticIp = false;
        IPAddress staIP;
        IPAddress staGW;
        IPAddress staSN;
        IPAddress staDNS;

        bool staAllowReconf = true;

        virtual void setMode(NM_MODE_ENUM mode, bool state){};
};
