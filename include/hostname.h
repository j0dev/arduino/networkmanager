#pragma once

#include <stdio.h>
#include <Arduino.h>
#include <string.h>

void append(char* s, char c);
void int2hex(unsigned int num, char* buf);
const char* getHostName();