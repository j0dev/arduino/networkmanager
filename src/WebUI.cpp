#include "WebUI.h"

void NetworkManagerWebUI::pageAPSettings(String& page, JsonDocument* curConf) {
    JsonVariant conf = curConf->as<JsonVariant>();
    NetworkManagerWebUI::getHead(page, "Network - Device Manager");
    NetworkManagerWebUI::getBody(page, "Network Manager", "SoftAP / Standalone WiFi Settings");

    NetworkManagerWebUI::getFormStart(page, "/nm/config/ap");

    NetworkManagerWebUI::getFormCheckBtn(page, "enabled", "Active", conf["enabled"].as<bool>());
    NetworkManagerWebUI::getFormInput(page, "ssid", "SSID (Network name)", conf["ssid"].as<const char*>());
    NetworkManagerWebUI::getFormInput(page, "password", "Password", conf["password"].as<const char*>(), "Use 'keep' to not change the password", NM_HTML_ENUM_INPUT_TYPE::PASS);

    page.concat("<br/>");
    NetworkManagerWebUI::getFormCollapseStart(page, "a", "Advanced Settings");

    NetworkManagerWebUI::getFormCheckBtn(page, "staticIp", "Static IP", conf["staticIp"].as<bool>());
    NetworkManagerWebUI::getFormInput(page, "ipAddress", "IP Address", conf["ipAddress"].as<const char*>());
    NetworkManagerWebUI::getFormInput(page, "subnetMask", "Subnet Mask", conf["subnetMask"].as<const char*>());
    NetworkManagerWebUI::getFormInput(page, "gateway", "Gateway Address", conf["gateway"].as<const char*>());

    NetworkManagerWebUI::getFormCollapseEnd(page);
    NetworkManagerWebUI::getFormCollapseStart(page, "b", "Timeout Settings");

    NetworkManagerWebUI::getFormCheckBtn(page, "timeoutEnabled", "Timeout Active", conf["timeoutEnabled"].as<bool>());
    NetworkManagerWebUI::getFormInput(page, "timeout", "Timeout (in seconds)", conf["timeout"].as<uint>(), "Automatically gets canceled when opening Device Manager", NM_HTML_ENUM_INPUT_TYPE::NUM);

    NetworkManagerWebUI::getFormCollapseEnd(page);

    page.concat("<br/><br/>");
    NetworkManagerWebUI::getBtn(page, "#", "Save Network Config", "submit");

    NetworkManagerWebUI::getFormEnd(page);

    page.concat("<br/>");
    NetworkManagerWebUI::getBtn(page, "/nm", "Return");
    NetworkManagerWebUI::getTail(page);
}

void NetworkManagerWebUI::pageSTASettings(String& page, JsonDocument* curConf) {
    JsonVariant conf = curConf->as<JsonVariant>();
    NetworkManagerWebUI::getHead(page, "Network - Device Manager");
    NetworkManagerWebUI::getBody(page, "Network Manager", "WiFi Client Settings");

    NetworkManagerWebUI::getFormStart(page, "/nm/config/sta");

    NetworkManagerWebUI::getFormCheckBtn(page, "enabled", "Active", conf["enabled"].as<bool>());
    NetworkManagerWebUI::getFormInput(page, "ssid", "SSID (Network name)", conf["ssid"].as<const char *>());
    NetworkManagerWebUI::getFormInput(page, "password", "Password", conf["password"].as<const char *>(), "Use 'keep' to not change the password", NM_HTML_ENUM_INPUT_TYPE::PASS);
#ifdef NM_EAP_ENABLED
    NetworkManagerWebUI::getFormInput(page, "username", "Username", conf["username"].as<const char *>(), "Use only if your network requires an additional username / eap authentication", NM_HTML_ENUM_INPUT_TYPE::TEXT);
#endif

    page.concat("<br/>");
    NetworkManagerWebUI::getFormCollapseStart(page, "a", "Advanced Settings");

    NetworkManagerWebUI::getFormCheckBtn(page, "staticIp", "Static IP", conf["staticIp"].as<bool>());
    NetworkManagerWebUI::getFormInput(page, "ipAddress", "IP Address", conf["ipAddress"].as<const char *>());
    NetworkManagerWebUI::getFormInput(page, "subnetMask", "Subnet Mask", conf["subnetMask"].as<const char *>());
    NetworkManagerWebUI::getFormInput(page, "gateway", "Gateway Address", conf["gateway"].as<const char *>());
    NetworkManagerWebUI::getFormInput(page, "dns", "DNS Server", conf["dns"].as<const char *>());

    NetworkManagerWebUI::getFormCollapseEnd(page);

    page.concat("<br/><br/>");
    NetworkManagerWebUI::getBtn(page, "#", "Save Network Config", "submit");

    NetworkManagerWebUI::getFormEnd(page);

    page.concat("<br/>");
    NetworkManagerWebUI::getBtn(page, "/nm", "Return");
    NetworkManagerWebUI::getTail(page);
}


void NetworkManagerWebUI::getHead(String& p) {
    NetworkManagerWebUI::getHead(p, "Device Manager");
}
void NetworkManagerWebUI::getHead(String& p, const char* title) {
    String html = FPSTR(NM_HTML_HEAD);
    html.replace("{a}", title);
    p.concat(html);
}
void NetworkManagerWebUI::getBody(String& p, const char* header, const char* subHeader) {
    String html = FPSTR(NM_HTML_BODY);
    html.replace("{a}", header);
    html.replace("{b}", subHeader);
    p.concat(html);
}
void NetworkManagerWebUI::getTail(String& p) {
    p.concat(FPSTR(NM_HTML_END));
}


void NetworkManagerWebUI::getFormStart(String& p, const char* action) {
    String html = FPSTR(NM_HTML_FORM_START);
    html.replace("{a}", action);
    p.concat(html);
}
void NetworkManagerWebUI::getFormEnd(String& p) {
    p.concat(FPSTR(NM_HTML_FORM_END));
}


void NetworkManagerWebUI::getFormCheckBtn(String& p, const char* variable, const char* text, bool checked) {
    String html = FPSTR(NM_HTML_FORM_CHECKBTN);
    html.replace("{a}", variable);
    html.replace("{b}", text);
    if (checked) {
        html.replace("{c}", "checked");
    } else {
        html.replace("{c}", "");
    }
    p.concat(html);
}


void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label) {
    NetworkManagerWebUI::getFormInput(p, variable, label, "", NM_HTML_ENUM_INPUT_TYPE::TEXT);
}
void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, NM_HTML_ENUM_INPUT_TYPE type) {
    NetworkManagerWebUI::getFormInput(p, variable, label, "", type);
}

void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, int value, NM_HTML_ENUM_INPUT_TYPE type) {
    NetworkManagerWebUI::getFormInput(p, variable, label, value, "", type);
}
void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, int value, String note, NM_HTML_ENUM_INPUT_TYPE type) {
    char buffer[16];
    itoa(value, buffer, 10);
    NetworkManagerWebUI::getFormInput(p, variable, label, buffer, note, type);
}

void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, const char* value) {
    NetworkManagerWebUI::getFormInput(p, variable, label, value, NM_HTML_ENUM_INPUT_TYPE::TEXT);
}
void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, const char* value, NM_HTML_ENUM_INPUT_TYPE type) {
    NetworkManagerWebUI::getFormInput(p, variable, label, value, "", type);
}
void NetworkManagerWebUI::getFormInput(String& p, const char* variable, const char* label, const char* value, String note, NM_HTML_ENUM_INPUT_TYPE type) {
    String html = FPSTR(NM_HTML_FORM_INPUT);
    html.replace("{a}", variable);
    html.replace("{b}", label);
    html.replace("{c}", label); // TODO add overloads for separate placeholder
    html.replace("{d}", note);
    html.replace("{e}", value);
    String stype;
    inputEnumToString(type, stype);
    html.replace("{f}", stype);

    p.concat(html);
}

void NetworkManagerWebUI::getFormCollapseStart(String& p, const char* uid, const char* label) {
    String html = FPSTR(NM_HTML_FORM_COLLAPSE_START);
    html.replace("{a}", uid);
    html.replace("{b}", label);
    p.concat(html);
}
void NetworkManagerWebUI::getFormCollapseEnd(String& p) {
    p.concat(FPSTR(NM_HTML_FORM_COLLAPSE_END));
}
void NetworkManagerWebUI::getBtn(String& p, const char* url, const char* label) {
    NetworkManagerWebUI::getBtn(p, url, label, "");
}
void NetworkManagerWebUI::getBtn(String& p, const char* url, const char* label, const char* id) {
    String html = FPSTR(NM_HTML_BTN);
    html.replace("{a}", url);
    html.replace("{b}", label);
    html.replace("{c}", id);
    p.concat(html);
}

void NetworkManagerWebUI::inputEnumToString(NM_HTML_ENUM_INPUT_TYPE in, String& s) {
    switch (in) {
    case NM_HTML_ENUM_INPUT_TYPE::TEXT:
        s.concat("text");
        break;
    case NM_HTML_ENUM_INPUT_TYPE::PASS:
        s.concat("password");
        break;
    case NM_HTML_ENUM_INPUT_TYPE::NUM:
        s.concat("number");
        break;
    }
}