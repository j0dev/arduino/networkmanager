#include "WebManager.h"

#include <AsyncJson.h>
#ifndef NM_WEBUI_DISABLED
    #include "WebUI.h"
#endif
#include "NetworkManagerAp.h"
#include "NetworkManagerSta.h"

void NetworkWebManager::webInit(AsyncWebServer *server, uint16_t port, bool bindRoot) {
    this->server = server;
    this->port = port;
    this->webSetup(bindRoot);
}
void NetworkWebManager::webPointerInit(NetworkManagerAp *Ap, NetworkManagerSta *Sta) {
    this->WebApPointer = Ap;
    this->WebStaPointer = Sta;
}

void NetworkWebManager::webSetup(bool bindRoot) {
    // most specific routes go first because of matching system
    
    if (this->WebApPointer->isApReconfEnabled()) {
        #ifndef NM_WEBUI_DISABLED
        // ap config page
        this->server->on("/nm/ap", HTTP_GET, std::bind(&NetworkWebManager::webHandlePageAp, this, std::placeholders::_1));
        #endif
        // ap config api
        this->server->on("/nm/config/ap", HTTP_GET, std::bind(&NetworkWebManager::webHandleConfigApRead, this, std::placeholders::_1));
        AsyncCallbackJsonWebHandler *h1 = new AsyncCallbackJsonWebHandler("/nm/config/ap", std::bind(&NetworkWebManager::webHandleConfigApWrite, this, std::placeholders::_1, std::placeholders::_2));
        this->server->addHandler(h1);
    }
    
    if (this->WebStaPointer->isStaReconfEnabled()) {
        #ifndef NM_WEBUI_DISABLED
        // sta config page
        this->server->on("/nm/sta", HTTP_GET, std::bind(&NetworkWebManager::webHandlePageSta, this, std::placeholders::_1));
        #endif
        // sta config api
        this->server->on("/nm/config/sta", HTTP_GET, std::bind(&NetworkWebManager::webHandleConfigStaRead, this, std::placeholders::_1));
        AsyncCallbackJsonWebHandler *h2 = new AsyncCallbackJsonWebHandler("/nm/config/sta", std::bind(&NetworkWebManager::webHandleConfigStaWrite, this, std::placeholders::_1, std::placeholders::_2));
        this->server->addHandler(h2);
    }

    #ifndef NM_WEBUI_DISABLED
    // static assets
    this->server->on("/nm/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse_P(200, "text/css", style_css_gz, style_css_gz_len);
        response->addHeader("Content-Encoding", "gzip");
        request->send(response);
    });
    this->server->on("/nm/script.js", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse_P(200, "text/javascript", script_js_gz, script_js_gz_len);
        response->addHeader("Content-Encoding", "gzip");
        request->send(response);
    });

    // root (network manager)
    this->server->on("/nm", HTTP_GET, std::bind(&NetworkWebManager::webHandlePageRoot, this, std::placeholders::_1));
    
    // root (only when standalone webserver)
    if (bindRoot) {
        this->server->on("/", HTTP_GET, [](AsyncWebServerRequest *request){ request->redirect("/nm"); });
    }
    #endif
}

#ifndef NM_WEBUI_DISABLED
void NetworkWebManager::webHandlePageRoot(AsyncWebServerRequest *request) {
    String page((char *)0);
    page.reserve(900);
    NetworkManagerWebUI::getHead(page);
    NetworkManagerWebUI::getBody(page, "Network Manager", this->getHostname());
    NetworkManagerWebUI::getBtn(page, "/nm/gen", "General settings", "a disabled");
    if (this->WebStaPointer->isStaReconfEnabled()) {
        NetworkManagerWebUI::getBtn(page, "/nm/stas", "WiFi Settings (scan networks)", "a disabled");
        NetworkManagerWebUI::getBtn(page, "/nm/sta", "WiFi Settings (no scan)");
    }
    if (this->WebApPointer->isApReconfEnabled()) {
        NetworkManagerWebUI::getBtn(page, "/nm/ap", "WiFi SoftAP Settings (standalone)");
    }
    page.concat("<br/><br/>");
    NetworkManagerWebUI::getBtn(page, "/nm/ci", "Connection Info", "a disabled");
    NetworkManagerWebUI::getBtn(page, "/nm/di", "Device Info", "a disabled");
    page.concat("<br/><br/>");
    NetworkManagerWebUI::getBtn(page, "/nm/reconnect", "Reconnect network(s)", "a disabled");
    NetworkManagerWebUI::getBtn(page, "/nm/restart", "Restart Device", "a disabled");
    page.concat("<br/><br/>");
    NetworkManagerWebUI::getBtn(page, "/", "Exit / Retrun");
    NetworkManagerWebUI::getTail(page);
    request->send(200, "text/html", page);
}

void NetworkWebManager::webHandlePageAp(AsyncWebServerRequest *request) {
    String page((char *)0);
    page.reserve(3300);
    const size_t capacity = JSON_OBJECT_SIZE(NMAP_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    this->WebApPointer->apGetJson(doc);
    if (doc["password"] != "") {
        doc["password"] = "keep";
    }

    NetworkManagerWebUI::pageAPSettings(page, &doc);

    request->send(200, "text/html", page);
}

void NetworkWebManager::webHandlePageSta(AsyncWebServerRequest *request) {
    String page((char *)0);
    page.reserve(2900);
    const size_t capacity = JSON_OBJECT_SIZE(NMSTA_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    this->WebStaPointer->staGetJson(doc);
    if (doc["password"] != "") {
        doc["password"] = "keep";
    }

    NetworkManagerWebUI::pageSTASettings(page, &doc);

    request->send(200, "text/html", page);
}
#endif


void NetworkWebManager::webHandleConfigApRead(AsyncWebServerRequest *request) {
    String s((char *)0);

    const size_t capacity = JSON_OBJECT_SIZE(NMAP_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    this->WebApPointer->apGetJson(doc);
    if (doc["password"] != "") {
        doc["password"] = "keep";
    }

    serializeJson(doc, s);

    request->send(200, "application/json", s);
}

void NetworkWebManager::webHandleConfigApWrite(AsyncWebServerRequest *request, JsonVariant json) {
    this->WebApPointer->apLoadJson(json);
    
    if (!this->WebApPointer->apWriteConfig()) {
        request->send(400);
    }

    this->webHandleConfigApRead(request);
}

void NetworkWebManager::webHandleConfigStaRead(AsyncWebServerRequest *request) {
    String s((char *)0);

    const size_t capacity = JSON_OBJECT_SIZE(NMSTA_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    this->WebStaPointer->staGetJson(doc);
    if (doc["password"] != "") {
        doc["password"] = "keep";
    }

    serializeJson(doc, s);

    request->send(200, "application/json", s);
}

void NetworkWebManager::webHandleConfigStaWrite(AsyncWebServerRequest *request, JsonVariant json) {
    this->WebStaPointer->staLoadJson(json);
    
    if (!this->WebStaPointer->staWriteConfig()) {
        request->send(400);
    }

    this->webHandleConfigStaRead(request);
}