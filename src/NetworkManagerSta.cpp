#include "NetworkManagerSta.h"

void NetworkManagerSta::staEnable() {
    this->staEnabled = true;
    this->setMode(NM_MODE_ENUM::STA, true);
}
void NetworkManagerSta::staDisable() {
    this->staEnabled = false;
    this->setMode(NM_MODE_ENUM::STA, false);
}
bool NetworkManagerSta::isStaEnabled() {
    return this->staEnabled;
}


void NetworkManagerSta::staEnableReconf() {
    this->staAllowReconf = true;
}
void NetworkManagerSta::staDisableReconf() {
    this->staAllowReconf = false;
}
bool NetworkManagerSta::isStaReconfEnabled() {
    return this->staAllowReconf;
}


void NetworkManagerSta::staBegin() {
    // read configuration if enabled
    if (this->staAllowReconf) {
        this->staLoadConfig();
    }
}

void NetworkManagerSta::staLoop() {
    // nothing yet
    // TODO: connection monitoring / fallback triggering
}

void NetworkManagerSta::staStart() {
    if (this->staEnabled) {
        if (this->isModeActive(NM_MODE_ENUM::AP)) {
            WiFi.mode(WIFI_AP_STA);
        } else {
            WiFi.mode(WIFI_STA);
        }
        if (this->staUser.length() == 0) { // check if username is not set
            if (this->staPass == NULL || this->staPass.length() < 8) {
                WiFi.begin(this->staSSID.c_str());
            } else {
                WiFi.begin(this->staSSID.c_str(), this->staPass.c_str());
            }
        } else { // eap authentication
            #ifdef NM_EAP_ENABLED
                #ifdef ESP32
                    WiFi.begin(this->staSSID.c_str(), WPA2_AUTH_PEAP, this->staUser.c_str(), this->staUser.c_str(), this->staPass.c_str());
#endif
                #ifdef ESP8266
                        WiFi.mode(WIFI_STA); // STA EAP + AP does not properly work on esp8266
                    struct station_config wifi_config;
                    memset(&wifi_config, 0, sizeof(wifi_config));
                    strcpy((char*)wifi_config.ssid, this->staSSID.c_str());
                    wifi_station_set_config(&wifi_config);
                    wifi_station_clear_cert_key();
                    wifi_station_clear_enterprise_ca_cert();
                    wifi_station_set_wpa2_enterprise_auth(1);
                    wifi_station_set_enterprise_identity((uint8_t *)this->staUser.c_str(), strlen(this->staUser.c_str()));
                    wifi_station_set_enterprise_username((uint8_t *)this->staUser.c_str(), strlen(this->staUser.c_str()));
                    wifi_station_set_enterprise_password((uint8_t *)this->staPass.c_str(), strlen(this->staPass.c_str()));
                    wifi_station_connect();
                #endif
            #endif
        }

        // esp32 requires hostname to be set after begin
        WiFi.hostname(this->getHostname());

        if (this->staStaticIp) {
            WiFi.config(this->staIP, this->staGW, this->staSN, this->staDNS);
        }
    }
}


void NetworkManagerSta::setStaSSID(String ssid) {
    this->staSSID = ssid;
}
String NetworkManagerSta::getStaSSID() {
    return this->staSSID;
}
void NetworkManagerSta::setStaPass(String pass) {
    this->staPass = pass;
}
String NetworkManagerSta::getStaPass() {
    return this->staPass;
}
void NetworkManagerSta::setStaUser(String user) {
    this->staUser = user;
}
void NetworkManagerSta::unsetStaUser() {
    this->staUser.clear();
}
String NetworkManagerSta::getStaUser() {
    return this->staUser;
}
bool NetworkManagerSta::hasStaUser() {
    return !this->staUser.isEmpty();
}


void NetworkManagerSta::setStaStaticIP(IPAddress ip, IPAddress gw, IPAddress sn) {
    IPAddress dns(1,1,1,1);
    this->setStaStaticIP(ip, gw, sn, dns);
}
void NetworkManagerSta::setStaStaticIP(IPAddress ip, IPAddress gw, IPAddress sn, IPAddress dns) {
    this->staIP = ip;
    this->staGW = gw;
    this->staSN = sn;
    this->staDNS = dns;
    this->staStaticIp = true;
}
void NetworkManagerSta::unsetStaStaticIP() {
    this->staStaticIp = false;
}
IPAddress NetworkManagerSta::getStaIp() {
    return this->staIP;
}
IPAddress NetworkManagerSta::getStaGw() {
    return this->staGW;
}
IPAddress NetworkManagerSta::getStaSn() {
    return this->staSN;
}
IPAddress NetworkManagerSta::getStaDns() {
    return this->staDNS;
}
bool NetworkManagerSta::isStaStaticIpEnabled() {
    return this->staStaticIp;
}


bool NetworkManagerSta::staLoadConfig() {
    // create document
    const size_t capacity = JSON_OBJECT_SIZE(NMSTA_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    // read configuration
    bool read = fs_read_json_doc(NMSTA_CONFIG_FILE, &doc);
    if (!read) {
        return false;
    }

    // apply configuration
    this->staLoadJson(doc.as<JsonVariant>());

    return true;
}
void NetworkManagerSta::staLoadJson(JsonVariant conf) {
    if (conf.containsKey("enabled")) {
        this->staEnabled = conf["enabled"].as<bool>();
        this->setMode(NM_MODE_ENUM::STA, this->staEnabled);
    }
    if (conf.containsKey("ssid")) {
        this->staSSID = conf["ssid"].as<String>();
    }
    if (conf.containsKey("password") && conf["password"] != "keep") {
        this->staPass = conf["password"].as<String>();
    }
    if (conf.containsKey("username")) {
        this->staUser = conf["username"].as<String>();
    }
    if (conf.containsKey("staticIp")) {
        this->staStaticIp = conf["staticIp"].as<bool>();
    }
    if (conf.containsKey("ipAddress")) {
        this->staIP.fromString(conf["ipAddress"].as<const char*>());
    }
    if (conf.containsKey("gateway")) {
        this->staGW.fromString(conf["gateway"].as<const char*>());
    }
    if (conf.containsKey("subnetMask")) {
        this->staSN.fromString(conf["subnetMask"].as<const char*>());
    }
    if (conf.containsKey("dns")) {
        this->staDNS.fromString(conf["dns"].as<const char*>());
    }
}
void NetworkManagerSta::staGetJson(JsonDocument& doc) {
    doc["enabled"] = this->staEnabled;
    doc["ssid"] = this->staSSID;
    doc["password"] = this->staPass;
    doc["username"] = this->staUser;
    doc["staticIp"] = this->staStaticIp;
    doc["ipAddress"] = this->staIP.toString();
    doc["gateway"] = this->staGW.toString();
    doc["subnetMask"] = this->staSN.toString();
    doc["dns"] = this->staDNS.toString();
}
bool NetworkManagerSta::staWriteConfig() {
    // create document
    const size_t capacity = JSON_OBJECT_SIZE(NMSTA_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);
    // get configuration
    this->staGetJson(doc);
    // write configuration
    return fs_write_json_doc(NMSTA_CONFIG_FILE, &doc);
}
