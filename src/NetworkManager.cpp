#include "NetworkManager.h"

#if ESP32
    #include <WiFi.h>
#endif
#if ESP8266
    #include <ESP8266WiFi.h>
#endif

#include "hostname.h"

NetworkManager::NetworkManager() {
    // set default hostname
    this->hostnameSuffix = getHostName();
}

void NetworkManager::begin() {
    // set our hostname if not overriden
    if (this->hostname.length() == 0) {
        this->hostname.reserve(this->hostnamePrefix.length() + this->hostnameSuffix.length());
        this->hostname.concat(this->hostnamePrefix);
        this->hostname.concat(this->hostnameSuffix);
    }
    #ifdef WIFI
        WiFi.hostname(this->hostname);
        WiFi.setAutoConnect(false); // disable auto connect at boot??
        WiFi.persistent(false); // disable auto connect at boot without proper WiFi.begin()
    #endif

    this->apBegin();
    this->staBegin();

    #ifndef NM_WEB_DISABLED
        this->webPointerInit(this, this);
    #endif
}

void NetworkManager::start() {
    this->apStart();
    this->staStart();
}

void NetworkManager::loop() {
    this->apLoop();
    this->staLoop();
}

void NetworkManager::setHostname(const char* hostname) {
    this->hostname = hostname;
}
void NetworkManager::setHostnamePrefix(const char* prefix) {
    this->hostnamePrefix = prefix;
}
void NetworkManager::setHostnameSuffix(const char* hostname) {
    this->hostnameSuffix = hostname;
}

const char* NetworkManager::getHostname() {
    return this->hostname.c_str();
}

bool NetworkManager::isModeActive(NM_MODE_ENUM mode) {
    return (this->activeModes & mode) == mode;
}

void NetworkManager::setMode(NM_MODE_ENUM mode, bool state) {
    // only change if we need to
    if (this->isModeActive(mode) != state) {
        if (this->isModeActive(mode)) {
            this->activeModes -= mode;
        } else {
            this->activeModes += mode;
        }
    }
}

NetworkManager networkManager;