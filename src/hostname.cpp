#include "hostname.h"

// hostname prefix (+ allocation for the rest)
char hostname [7] = "";
bool hostnameLoaded = false;

void append(char* s, char c) {
    int len = strlen(s);
    s[len] = c;
    s[len+1] = '\0';
}

#ifdef ESP32
// endianness specific!!
void int2hex(unsigned int num, char* buf) {
  char chars [17] = "0123456789abcdef";
  while (num != 0) {
    uint8_t cur = num;
    uint8_t cha1 = num;
    uint8_t cha2 = cur >> 4;
    uint8_t tmp = cha2 << 4;
    cha1 -= tmp;
    append(buf, chars[cha2]);
    append(buf, chars[cha1]);
    num -= cur;
    num = num >> 8;
  }
}
#endif

#ifdef ESP8266
// endianness specific!!
void int2hex(unsigned int num, char* buf) {
  char buf2 [7] = {'\0'};
  char chars [17] = "0123456789abcdef";
  while (num != 0) {
    uint8_t cur = num;
    uint8_t cha1 = num;
    uint8_t cha2 = cur >> 4;
    uint8_t tmp = cha2 << 4;
    cha1 -= tmp;
    append(buf2, chars[cha1]);
    append(buf2, chars[cha2]);
    num -= cur;
    num = num >> 8;
  }
  for (int i = strlen(buf2); i >= 0; i--)
  {
    append(buf, buf2[i]);
  }
}
#endif

const char* getHostName() {
  if (!hostnameLoaded) {
    #ifdef ESP32
      uint64_t mac = ESP.getEfuseMac();
      uint64_t mac2 = mac >> 24;
      unsigned int mac3 = mac2;
      int2hex(mac3, hostname);
    #endif
    #ifdef ESP8266
      unsigned int mac = ESP.getChipId();
      int2hex(mac, hostname);
    #endif
    hostnameLoaded = true;
  }
  return hostname;
}