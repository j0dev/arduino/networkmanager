#include "config.h"

bool fs_init_begin() {
    bool success = LittleFS.begin();
    if (!success) {
        LittleFS.format();
        success = LittleFS.begin();
    }
    return success;
}

bool fs_read_json_doc(String filename, JsonDocument *doc) {
    if (!fs_init_begin()) return false;

    // check if file exists
    if (!LittleFS.exists(filename)) {
        return false;
    }

    // open file
    File f = LittleFS.open(filename, "r");
    if (!f) return false;

    // read into our document
    DeserializationError err = deserializeJson(*doc, f);
    if (err) return false;

    // close and return
    f.close();
    return true;
}

bool fs_read_json_doc(String folder, String filename, JsonDocument *doc) {
    if (!fs_init_begin()) return false;

    String newFilename = "/" + folder + "/" + filename;
    return fs_read_json_doc(newFilename, doc);
}

bool fs_write_json_doc(String filename, JsonDocument *doc) {
    if (!fs_init_begin()) return false;

    // open file
    File f = LittleFS.open(filename, "w");
    if (!f) return false;

    // write
    serializeJson(*doc, f);

    // close and return
    f.close();
    return true;
}

bool fs_write_json_doc(String folder, String filename, JsonDocument *doc) {
    if (!fs_init_begin()) return false;

    String newFilename = "/" + folder + "/" + filename;
    return fs_write_json_doc(newFilename, doc);
}