#include "NetworkManagerAp.h"

void NetworkManagerAp::apEnable() {
    this->apEnabled = true;
    this->setMode(NM_MODE_ENUM::AP, true);
}
void NetworkManagerAp::apDisable() {
    this->apEnabled = false;
    this->setMode(NM_MODE_ENUM::AP, false);
}
bool NetworkManagerAp::isApEnabled() {
    return this->apEnabled;
}


void NetworkManagerAp::apEnableReconf() {
    this->apAllowReconf = true;
}
void NetworkManagerAp::apDisableReconf() {
    this->apAllowReconf = false;
}
bool NetworkManagerAp::isApReconfEnabled() {
    return this->apAllowReconf;
}


void NetworkManagerAp::apBegin() {
    if (this->apSSID.length() == 0) {
        this->apSSID = this->getHostname();
    }
    
    // read configuration if enabled
    if (this->apAllowReconf) {
        this->apLoadConfig();
    }
}

void NetworkManagerAp::apLoop() {
    if (this->apEnabled) {
        if (this->apTimeoutActive && this->apTimeoutRunning) {
            if ((this->apTimeoutStart + (this->apTimeout * 1000)) < millis()) {
                this->apTimeoutRunning = false;
                WiFi.softAPdisconnect(false);
                if (this->isModeActive(NM_MODE_ENUM::STA)) {
                    WiFi.mode(WIFI_STA);
                } else {
                    WiFi.mode(WIFI_OFF);
                }
                this->setMode(NM_MODE_ENUM::AP, false);
            }
        }
    }
}

void NetworkManagerAp::apStart() {
    if (this->apEnabled) {
        if (this->isModeActive(NM_MODE_ENUM::STA)) {
            WiFi.mode(WIFI_AP_STA);
        } else {
            WiFi.mode(WIFI_AP);
        }
        if (this->apPSK == NULL || this->apPSK.length() < 8) {
            WiFi.softAP(this->apSSID.c_str());
        } else {
            WiFi.softAP(this->apSSID.c_str(), this->apPSK.c_str());
        }

        if (this->apStaticIp) {
            delay(100); // TODO: figure out a more appropiate delay
            WiFi.softAPConfig(this->apIP, this->apGW, this->apSN);
        }

        this->resetApTimeout();
    }
}


void NetworkManagerAp::setApSSID(String ssid) {
    this->apSSID = ssid;
}
String NetworkManagerAp::getApSSID() {
    return this->apSSID;
}
void NetworkManagerAp::setApPSK(String psk) {
    this->apPSK = psk;
}
String NetworkManagerAp::getApPSK() {
    return this->apPSK;
}


void NetworkManagerAp::setApStaticIP(IPAddress ip, IPAddress gw, IPAddress sn) {
    this->apIP = ip;
    this->apGW = gw;
    this->apSN = sn;
    this->apStaticIp = true;
}
void NetworkManagerAp::unsetApStaticIP() {
    this->apStaticIp = false;
}

IPAddress NetworkManagerAp::getApIp() {
    return this->apIP;
}
IPAddress NetworkManagerAp::getApGw() {
    return this->apGW;
}
IPAddress NetworkManagerAp::getApSn() {
    return this->apSN;
}
bool NetworkManagerAp::isApStaticIpEnabled() {
    return this->apStaticIp;
}


void NetworkManagerAp::setApTimeout(uint timeout) {
    if (timeout == 0) {
        this->apTimeoutActive = false;
    } else {
        this->apTimeout = timeout;
        this->apTimeoutActive = true;
    }
}

uint NetworkManagerAp::getApTimeout() {
    return this->apTimeout;
}
bool NetworkManagerAp::isApTimeoutActive() {
    return this->apTimeoutActive;
}
bool NetworkManagerAp::isApTimeoutRunning() {
    return this->apTimeoutRunning;
}

void NetworkManagerAp::stopApTimeout() {
    if (this->apTimeoutActive) {
        this->apTimeoutRunning = false;
    }
}
void NetworkManagerAp::resetApTimeout() {
    if (this->apTimeoutActive) {
        this->apTimeoutRunning = true;
        this->apTimeoutStart = millis();
    }
}


bool NetworkManagerAp::apLoadConfig() {
    // create document
    const size_t capacity = JSON_OBJECT_SIZE(NMAP_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);

    // read configuration
    bool read = fs_read_json_doc(NMAP_CONFIG_FILE, &doc);
    if (!read) {
        return false;
    }

    // apply configuration
    this->apLoadJson(doc.as<JsonVariant>());

    return true;
}
void NetworkManagerAp::apLoadJson(JsonVariant conf) {
    // JsonVariant conf = doc->as<JsonVariant>();
    if (conf.containsKey("enabled")) {
        this->apEnabled = conf["enabled"].as<bool>();
        this->setMode(NM_MODE_ENUM::AP, this->apEnabled);
    }
    if (conf.containsKey("ssid")) {
        this->apSSID = conf["ssid"].as<String>();
    }
    if (conf.containsKey("password") && conf["password"] != "keep") {
        this->apPSK = conf["password"].as<String>();
    }
    if (conf.containsKey("staticIp")) {
        this->apStaticIp = conf["staticIp"].as<bool>();
    }
    if (conf.containsKey("ipAddress")) {
        this->apIP.fromString(conf["ipAddress"].as<const char*>());
    }
    if (conf.containsKey("gateway")) {
        this->apGW.fromString(conf["gateway"].as<const char*>());
    }
    if (conf.containsKey("subnetMask")) {
        this->apSN.fromString(conf["subnetMask"].as<const char*>());
    }
    if (conf.containsKey("timeout")) {
        this->apTimeout = conf["timeout"].as<uint>();
    }
    if (conf.containsKey("timeoutEnabled")) {
        this->apTimeoutActive = conf["timeoutEnabled"].as<bool>();
    }
}
void NetworkManagerAp::apGetJson(JsonDocument& doc) {
    doc["enabled"] = this->apEnabled;
    doc["ssid"] = this->apSSID;
    doc["password"] = this->apPSK;
    doc["staticIp"] = this->apStaticIp;
    doc["ipAddress"] = this->apIP.toString();
    doc["gateway"] = this->apGW.toString();
    doc["subnetMask"] = this->apSN.toString();
    doc["timeout"] = this->apTimeout;
    doc["timeoutEnabled"] = this->apTimeoutActive;
}
bool NetworkManagerAp::apWriteConfig() {
    // create document
    const size_t capacity = JSON_OBJECT_SIZE(NMAP_CONFIG_CAP);
    DynamicJsonDocument doc(capacity);
    // get configuration
    this->apGetJson(doc);
    // write configuration
    return fs_write_json_doc(NMAP_CONFIG_FILE, &doc);
}
