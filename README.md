ESP Network Manager
======

ESP Network Manager is an Arduino library to manage the network configuration of ESP micro-controllers.
Both through a programming interface as well as a web interface


## Supported platforms
- ESP8266
- ESP32


## Features
Note: features might not be fully implemented until v1.0.0

Network types:
- WiFi AP (softAP)
- WiFi (STA) Client
- WiFi (STA) Client EAP authentication
- Ethernet

Features:
- Allow configuration from any network type
- AP auto-disable timeout (can be canceled)
- AP fallback mode when disconnected from other networks
- Hostname configuration
- IP configuration (both static and DHCP*)
- mDNS NetworkManager auto-discovery
- ntp configuration
- Web configuration portal (mobile friendly)
- Password protection
- Persistent configuration storage (can be overridden and locked out by sketch)
- Programmatic extension of web interfaces

*: Some platforms might have issues with dhcp (esp8266 with ethernet controllers are known for this)