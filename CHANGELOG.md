# ESP NetworkManager Changelog
For an up-to-date overview of implemented features and documentation, please see the feature-matrix chart and documentation on the repositories wiki / pages.

Every changelog entry is prefixed with a change type between brackets. These will indicate what type and for who this changelog entry might be useful.  
- General might be interesting for anyone, even end-users. They outline bigger fixes, improvements and new functionality.
- Dev will be interesting to developers integrating this library. This outlines changes in how things work, API changes and additions.
- Internal will be interesting to developers maintaining or developing modules that tightly integrate with the library. This outlines internal changes.


## v0.6.?
- [dev] webUI - Multiple forms now work correctly on a single page
- [dev] webUI - All 2xx status codes now trigger the success popover
- [internal] webUI - Some minimizing on resources

## v0.6.5
- [internal] Updated EAP WiFi setup (ESP32)
- [internal] Updated ArduinoJSON (and typecasts)
- [internal] Updated LittleFS (ESP32 now uses the build in version)

## v0.6.3
- [general] Fixed setting correct WiFi modes depending on other modules state (AP + STA)
- [internal] Added generic functions for getting and setting module states (that also works internally in modules)

## v0.6.2
- [general] Updated how the hostname is generated
- [dev] Added function to change the prefix of the hostname
- [dev] Added function to override the generated part of a hostname (usually the MAC-address)

## v0.6.1
- [dev] Fixed the ESP32 LittleFS dependency
- [internal] Changed the dependency format to the extended version

## v0.6.0
- [general] Implemented WPA2-EAP authentication
- [general] Changed username field to text field instead of password
- [dev] WiFi client EAP authentication is now available by setting `NM_EAP_ENABLED` build flag

## v0.5.1
- [general] Fixed PIO package `authors` field

## v0.5.0
- [general] Initial publication of PIO Package
